#include <iostream>
#include <list>
#include <string>

using std::list;
using std::string;

struct Osoba {
	int redniBroj;
	string ime;
	string prezime;
};

class Cekaonica {
public:
	void dodajNaCekanje(string ime, string prezime) {
		osobe.push_back({ static_cast<int>(osobe.size()) + 1, ime, prezime });
	}
	void tkoCeka() {
		/*for (Osoba osoba : osobe) {
			std::cout << osoba.redniBroj << " " << osoba.ime << " " << osoba.prezime << std::endl;
		}*/

		for (auto it = osobe.begin(); it != osobe.end(); it++) {
			std::cout << it->redniBroj << " " << it->ime << " " << it->prezime << std::endl;
		}
	}
	Osoba* sljedeci() {
		if (osobe.size() > 0) {
			return &osobe.front();
		}
		return nullptr;
	}
	Osoba preuzmiSljedeceg() {
		if (osobe.size() > 0) {
			Osoba o = osobe.front();
			osobe.pop_front();
			return o;
		}
		return { -1, "", "" };
		// exception 
	}
  
private:
	list<Osoba> osobe;
};

int main() {
	Cekaonica cekaonica;
	cekaonica.dodajNaCekanje("Hrvoje", "Horvat");
	cekaonica.dodajNaCekanje("Marko", "Markic");
	cekaonica.dodajNaCekanje("Ivan", "Ivic");

  cekaonica.tkoCeka();

	Osoba preuzet = cekaonica.preuzmiSljedeceg();
	std::cout << "Preuzet: " << preuzet.prezime << std::endl;

	cekaonica.tkoCeka();
}