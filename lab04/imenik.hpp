#include <vector>
#include "osoba.hpp"

class Imenik {
public:
  void unesiOsobu(Osoba osoba);
  Osoba trazi(std::string broj);
  Osoba trazi(std::string ime, std::string prezime);
  int brojZapisa();
  void prikazZapisa();

private:
  std::vector<Osoba> osobe;
};