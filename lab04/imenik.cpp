#include <iostream>
#include "imenik.hpp"

void Imenik::unesiOsobu(Osoba osoba) {
  osobe.push_back(osoba);
}

Osoba Imenik::trazi(std::string broj) {
  for (const auto &osoba : osobe) {
    if (osoba.broj == broj) {
      return osoba;
    }
  }
  return { "", "", "" };
}

Osoba Imenik::trazi(std::string ime, std::string prezime) {
  for (const auto &osoba : osobe) {
    if (osoba.ime == ime && osoba.prezime == prezime) {
      return osoba;
    }
  }
  return { "", "", "" };
}

int Imenik::brojZapisa() {
  return osobe.size();
}

void Imenik::prikazZapisa() {
  for (auto it = osobe.begin(); it != osobe.end(); it++) {
    std::cout << it->ime << " ";
    std::cout << it->prezime << ", ";
    std::cout << it->broj << std::endl;
  }
}
