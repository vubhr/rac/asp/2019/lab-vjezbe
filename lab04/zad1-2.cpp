#include <iostream>
#include "imenik.hpp"

int main() {
  Imenik imenik;
  imenik.unesiOsobu({ "Hrvoje", "Horvat", "0912222333" });
  imenik.unesiOsobu({ "Marko", "Ivic", "0983333222" });

  Osoba trazena = imenik.trazi("Hrvoje", "Horvat");
  std::cout << trazena.broj << std::endl;

  trazena = imenik.trazi("0983333222");
  std::cout << trazena.prezime << std::endl;

  std::cout << "Broj zapisa: " << imenik.brojZapisa() << std::endl;

  imenik.prikazZapisa();
}