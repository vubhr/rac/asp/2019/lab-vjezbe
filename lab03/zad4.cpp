#include <iostream>

using std::cout;
using std::endl;

int prebroji2(int p[], int n) {
  if (n > 0) {
    if (p[n-1] >= 0) {
      return 1 + prebroji2(p, n - 1);
    } else {
      return prebroji2(p, n - 1);
    }
  }
  return 0;
}

int main() {
  int polje[5] = { -2, 3, -6, 8, 5 };
  int pozitivni = prebroji2(polje, 5);

  cout << pozitivni;
}