#include <iostream>

using std::cout;
using std::endl;

int sumaX(int p[], int n, int x) {
  if (n > 0) {
    if (p[n-1] == x) {
      return p[n-1] + sumaX(p, n - 1, x);
    } else {
      return sumaX(p, n - 1, x);
    }
  }
  return 0;
}

int main() {
  int polje[5] = { -2, 3, -6, 8, 5 };
  int suma = sumaX(polje, 5, 5);

  cout << suma;
}