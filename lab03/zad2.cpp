#include <iostream>

using std::cout;
using std::endl;

void parni(int n, int m) {
  if (n <= m) {
    if (n % 2 == 0) {
      cout << n;
      parni(n + 2, m);
    } else {
      parni(n + 1, m);
    }
  }
  return;
}

int main() {
  parni(3, 10);
}