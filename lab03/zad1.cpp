#include <iostream>

using std::cout;
using std::endl;

void znamenkeObrnuto(int n) {
  if (n > 0) {
    cout << n % 10;
    znamenkeObrnuto(n / 10);
  }
  return;
}

int main() {
  znamenkeObrnuto(365);
}