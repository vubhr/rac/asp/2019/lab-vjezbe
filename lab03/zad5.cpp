#include <iostream>

using std::cout;
using std::endl;

int prebroji3(int p[], int n, int k, int m) {
  if (n > 0) {
    if (p[n-1] >= k && p[n-1] <= m) {
      return 1 + prebroji3(p, n - 1, k, m);
    } else {
      return prebroji3(p, n - 1, k, m);
    }
  }
  return 0;
}

int main() {
  int polje[5] = { -2, 3, -6, 8, 5 };
  int pozitivni = prebroji3(polje, 5, 5, 10);

  cout << pozitivni;
}