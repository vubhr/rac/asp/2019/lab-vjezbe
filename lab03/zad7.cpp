#include <iostream>

using std::cout;
using std::endl;

int maksRekurzivno(int p[], int n, int max) {
  if (n > 0) {
    if (p[n-1] > max) {
      return maksRekurzivno(p, n - 1, p[n-1]);
    } else {
      return maksRekurzivno(p, n - 1, max);
    }
  }
  return max;
}

int maks(int p[], int n) {
  return maksRekurzivno(p, n, p[n-1]);
}

int main() {
  int polje[5] = { -2, 3, -6, 8, 5 };
  int max = maks(polje, 5);

  cout << max;
}