#include <iostream>

using std::cout;
using std::endl;

int br_znam(int n) {
  if (n == 0) {
    return 0;
  }
  return 1 + br_znam(n / 10);
}

int main() {
  cout << br_znam(387);
}