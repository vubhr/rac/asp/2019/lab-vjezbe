#include <iostream>

using std::cout;
using std::endl;

void prikazi2(int n) {
  if (n == 0) {
    return;
  }
  cout << n << endl;
  prikazi2(n - 1);
}

int main() {
  prikazi2(5);
}