#include <iostream>

using std::cout;
using std::endl;

void prikazi1(int n) {
  if (n == 0) {
    return;
  }
  prikazi1(n - 1);
  cout << n << endl;
}

int main() {
  prikazi1(5);
}