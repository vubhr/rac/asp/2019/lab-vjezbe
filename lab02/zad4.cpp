#include <iostream>

using std::cout;
using std::endl;

int suma2(int n) {
  if (n <= 0) {
    return 0;
  }
  return n * 2 + suma2(n - 1);
}

int main() {
  cout << suma2(3);
}