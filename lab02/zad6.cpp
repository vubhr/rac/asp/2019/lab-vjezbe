#include <iostream>

using std::cout;
using std::endl;

int zbroj_znam(int n) {
  if (n == 0) {
    return 0;
  }
  return n % 10 + zbroj_znam(n / 10);
}

int main() {
  cout << zbroj_znam(387);
}